// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import ElementUI from 'element-ui'; // 引入ElementUI核心JS
import 'element-ui/lib/theme-chalk/index.css'; // 引入ElementUI核心样式
Vue.use(ElementUI) // 告诉Vue我要使用ElementUI

Vue.config.productionTip = false
//导入user.js
// import MockUser from './js/user.js'

//引入axios
import axios from 'axios'
//配置axios的全局基本路径
axios.defaults.baseURL='https://www.fastmock.site/mock/ce566a320588cb697ee816dd3a3db1b2/element-study'
//全局属性配置，在任意组件内可以使用this.$http获取axios对象
Vue.prototype.$http = axios

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
