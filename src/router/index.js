import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import StudySinglePage from '@/components/StudySinglePage'
import MyButton from '@/components/MyButton'
import MyTable from '@/components/MyTable'
import User from '@/components/User.vue'
import Department from '@/components/Department'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/singlePage',
      name: 'studySinglePage',
      component: StudySinglePage
    },
    {
      path: '/mybutton',
      name: 'MyButton',
      component: MyButton
    },
    {
      path: '/myTable',
      name: 'MyTable',
      component: MyTable
    },
    {
      path: '/user',
      name: 'User',
      component: User
    },
    {
      path: '/dept',
      name: 'Department',
      component: Department
    }
  ]
})
